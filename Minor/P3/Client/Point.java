package Minor.P3.Client;

import Minor.P3.DS.Compare2D;
import Minor.P3.DS.Direction;

public class Point implements Compare2D<Point> {

	private long xcoord;
	private long ycoord;

	public Point() {
		xcoord = 0;
		ycoord = 0;
	}

	public Point(long x, long y) {
		xcoord = x;
		ycoord = y;
	}

	public long getX() {
		return xcoord;
	}

	public long getY() {
		return ycoord;
	}

	public Direction directionFrom(long X, long Y) {
		long xDiff = this.xcoord - X;
		long yDiff = this.ycoord - Y;
		if (X == this.xcoord && Y == this.ycoord) {
			return Direction.NOQUADRANT;
		} else if (xDiff >= 0) {
			if (yDiff >= 0) {
				return Direction.NE;
			} else {
				return Direction.SE;
			}
		} else { // xdiff < 0
			if (yDiff >= 0) {
				return Direction.NW;
			} else {
				return Direction.SW;
			}
		}
	}

	public Direction inQuadrant(double xLo, double xHi, double yLo, double yHi) {
		double yAxisLoc = (xLo + xHi) / 2.0;
		double xAxisLoc = (yLo + yHi) / 2.0;
		double x = (double) this.xcoord;
		double y = (double) this.ycoord;
		if (!inBox(xLo, xHi, yLo, yHi)) {
			return Direction.NOQUADRANT;
		} else if (x > yAxisLoc && y >= xAxisLoc) {
			return Direction.NE;
		} else if (x <= yAxisLoc && y > xAxisLoc) {
			return Direction.NW;
		} else if (x < yAxisLoc && y <= xAxisLoc) {
			return Direction.SW;
		} else if (x >= yAxisLoc && y < xAxisLoc) {
			return Direction.SE;
		}

		return Direction.NOQUADRANT;
	}

	public boolean inBox(double xLo, double xHi, double yLo, double yHi) {
		// xLo <= this.xcoord <= this.xHi
		// yLo <= this.ycoord <= this.yHi
		double yLo2 = yLo;
		double yHi2 = yHi;
		double xLo2 = xLo;
		double xHi2 = xHi;
		if (yLo > yHi) { // negative numbers probably
			yLo2 = yHi;
			yHi2 = yLo;
		}
		if (xLo > xHi) {
			xLo2 = xHi;
			xHi2 = xLo;
		}
		return (this.getX() >= xLo2 && this.getX() <= xHi2)
				&& (this.getY() >= yLo2 && this.getY() <= yHi2);
	}

	public String toString() {
		return "(" + this.getX() + ", " + this.getY() + ")";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (xcoord != other.xcoord)
			return false;
		if (ycoord != other.ycoord)
			return false;
		return true;
	}

}
